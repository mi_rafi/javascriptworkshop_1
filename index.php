<!--PHP Stuff-->
<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $contacts = json_decode(file_get_contents('data/Contact.json'));
?>


<!--
        HTML STARTS FROM HERE
-->
<html>
    <head>
        <script src="js/core/jquery-2.1.0.min.js" ></script>
    </head>
    <body>
        <div align="center" >Filter</div>
        <table  border="1" align="center">
            <tr>
                <td>Company</td>
                <td>
                    <select id="company_selector">
                        <option>All</option>
                        <option>GitHub</option>
                        <option>Instagram</option>
                        <option>Google</option>
                        <option>Yahoo</option>
                    </select>
                </td>
            </tr>
        </table>
        <table  border="1" align="center">


            <?php
                    foreach($contacts as $row) {

            ?>
                        <tr>

                            <td><?php echo $row->firstName ?>&nbsp;<?php echo $row->lastName ?></td>
                            <td><?php echo $row->company ?>&nbsp;<?php echo $row->company ?></td>
                            <td><?php echo $row->email ?></td>
                            <td>
                                <ul>
                                    <?php foreach($row->phoneNumbers as $number){ ?>
                                            <li style="padding-bottom:5px" >
                                                <span><?php echo $number; ?></span>&nbsp;
                                                <span style="cursor: pointer;border: solid 1px;">x</span>
                                            </li>
                                    <?php
                                        }
                                    ?>
                                <ul>
                            </td>
                            <td><input type="button" value="remove"/></td>
                            <td><input type="button" value="Edit"/></td>
                        </tr>
                        <input id="json_obj_<?php echo $row->id; ?>" type="hidden" value="<?php echo json_encode($row)?>" />
           <?php

                    }
            ?>
        </table>
        <div style="margin-top:20px">
            <table id="contact_edit" border="1" align="center">
                <tr>
                    <td>First Name : </td>
                    <td><input type="text" value="" /></td>
                </tr>
                <tr>
                    <td>Last Name : </td>
                    <td><input type="text" value="" /></td>
                </tr>
                <tr>
                    <td>Company Name : </td>
                    <td><input type="text" value="" /></td>
                </tr>
                <tr>
                    <td>Email : </td>
                    <td><input type="text" value="" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="button" value="Submit" /></td>
                </tr>
            </table>
        </div>

    </body>

    <script>
        $(document).ready(function(){

        });
    </script>

    <style>

        .trBackgroud{
            background : aliceblue;
        }
    </style>
</html>
